using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderDSG : MonoBehaviour
{
    [SerializeField] private GameObject _mainScreenDSG;
    
    private void Awake()
    {
        if (_mainScreenDSG != null)
            StartCoroutine(ProgressBarLoaderCoroutineDSG());
        
        bool refactorDSG = false;
        bool trashVariableForRefactoingDSG1 = refactorDSG;
        refactorDSG = true;
    }

    private IEnumerator ProgressBarLoaderCoroutineDSG()
    {
        yield return new WaitForSeconds(3f);

        gameObject.SetActive(false);
        _mainScreenDSG.SetActive(true);

        yield break;
    }
}

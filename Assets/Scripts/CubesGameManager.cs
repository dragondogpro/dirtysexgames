using DanielLochner.Assets.SimpleScrollSnap;
using LeTai.TrueShadow;
using UnityEngine;
using UnityEngine.UI;

public class CubesGameManager : MonoBehaviour
{
        [SerializeField] private SimpleScrollSnap _partOfTheBodyScrollSnap;
        [SerializeField] private SimpleScrollSnap _actionScrollSnap;
        
        [SerializeField] private Transform _nextSpinButton;

        private void Awake()
        {
                _actionScrollSnap.OnPanelCentered.AddListener(delegate { NextSpinDSG(); });
                _partOfTheBodyScrollSnap.OnPanelCentered.AddListener(delegate { NextSpinDSG(); });
        }
        
        private void NextSpinDSG()
        {
                _nextSpinButton.GetComponent<Button>().interactable = true;
                _nextSpinButton.GetComponent<TrueShadow>().enabled = true;
        }
}
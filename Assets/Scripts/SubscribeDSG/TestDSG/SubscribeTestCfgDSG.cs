﻿using System;
using UnityEngine;

namespace SubscribeDSG.TestDSG
{
    [Serializable]
    internal class SubscribeTestCfgDSG
    {
        [Header("Startup statesDSG")]
        [Tooltip("User already has any subscribe / User has't subscribeDSG ")]
        [SerializeField] private bool userHasSubscribeDSG;

        [Tooltip("User start application from push / Usually startDSG")]
        [SerializeField] private bool loadFromPushDSG;

        [Header("User actionsDSG")]
        [Tooltip("User cancel purchase, when buy productDSG")]
        [SerializeField] private bool userCancelPurchaseDSG;


        public bool UserHasSubscribeDsg => userHasSubscribeDSG;
        public bool LoadFromPushDsg => loadFromPushDSG;
        public bool UserCancelPurchaseDsg => userCancelPurchaseDSG;
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    internal class DefaultLoadDsgDsg : MonoBehaviour, IStoreLoadDSG
    {
        private const string mainSceneNameDSG = "SimpleScene";

        [SerializeField] private GameObject subscribePageDSG;
        [SerializeField] private OnSubscribeLoadDSG subscribeLoadDsg;

        public void OnLoadApplicationDSG()
        {
            LoadApplicationDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnLoadSubscribeDSG()
        {
            subscribePageDSG.SetActive(true);
            subscribeLoadDsg.LoadDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

#if !UNITY_EDITOR
        private static bool loadDSG = false;
#endif
        public static void LoadApplicationDSG()
        {
#if !UNITY_EDITOR
            if (loadDSG)
                return;

            loadDSG = true;
#endif

            SceneManager.LoadScene(mainSceneNameDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

using LeTai.TrueShadow;
using UnityEngine;
using UnityEngine.UI;

public class SubscriptionElementSelectedDSG : MonoBehaviour
{
    [SerializeField] private Color _defaultColorDSG;
    [SerializeField] private Color _selectedColorDSG;
    [SerializeField] private Color _defaultImageColorDSG;
    [SerializeField] private Transform _elementTextDGS;

    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(ChangeElementSizeDSG);
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }

    private void ChangeElementSizeDSG()
    {
        if (gameObject.GetComponent<RectTransform>().sizeDelta.x == 190)
        {
            gameObject.GetComponent<Image>().color = Color.white;
            gameObject.GetComponent<TrueShadow>().Color = _selectedColorDSG;
            _elementTextDGS.GetComponent<TrueShadow>().Color = _selectedColorDSG;
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(210, 210);
        }
        else
        {
            gameObject.GetComponent<Image>().color = _defaultImageColorDSG;
            gameObject.GetComponent<TrueShadow>().Color = Color.black;
            _elementTextDGS.GetComponent<TrueShadow>().Color = _defaultColorDSG;
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(190, 190);
        }
        
        LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }
    
    private void RefMethodDSG()
    {
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }
}

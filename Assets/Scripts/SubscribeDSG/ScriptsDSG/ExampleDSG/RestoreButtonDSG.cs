﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    [RequireComponent(typeof(Button))]
    internal class RestoreButtonDSG : MonoBehaviour
    {
        private Button buttonDSG;

        [SerializeField] private SubscribeControllerDSG subscribeDSG;

        void Awake()
        {
            buttonDSG = this.GetComponent<Button>();
            buttonDSG.onClick.AddListener(RestoreDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async void RestoreDSG()
        {
            buttonDSG.interactable = false;

            try
            {
                await subscribeDSG.RestoreDSG();
            }
            finally
            {
                if(buttonDSG != null)
                {
                    buttonDSG.interactable = true;
                }
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

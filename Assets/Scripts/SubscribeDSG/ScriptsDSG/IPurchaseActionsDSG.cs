﻿namespace SubscribeDSG.ScriptsDSG
{
    /// <summary>
    /// This interface should implement an action when the user interacts with the native AppStore window, initialized method group "Buy"
    /// </summary>
    internal interface IPurchaseActionsDSG
    {
        /// <summary>
        /// Сalled when user success buy a subscription
        /// </summary>
        void OnPurchaseDSG();

        /// <summary>
        /// Сalled when user decline buy a subscription
        /// </summary>
        void OnFaliedDSG();
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DanielLochner.Assets.SimpleScrollSnap;
using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomCubesController : MonoBehaviour
{
    [SerializeField] private Button _goSpinButtonDSG;
    [SerializeField] private Transform _rightUIElement;
    [SerializeField] private Transform _leftUIElement;
    [SerializeField] private Transform _rightScrollPanelDSG;
    [SerializeField] private Transform _leftScrollPanelDSG;

    [SerializeField] private List<TMP_InputField> _inputRightFieldsDSG;
    [SerializeField] private List<TMP_InputField> _inputLeftFieldsDSG;
    
    [SerializeField] private List<Transform> _prefabsListOnRightScrollDSG;
    [SerializeField] private List<Transform> _prefabsListOnLeftScrollDSG;

    private List<string> _actionListDSG = new List<string>();
    private List<string> _partOfTheBodyListDSG = new List<string>();
    private int countFieldsDSG;
    
    void Start()
    {
        _goSpinButtonDSG.onClick.AddListener(SetNewListDSG);
        _rightScrollPanelDSG.GetComponent<SimpleScrollSnap>().OnPanelCentered.AddListener(delegate
        {
            ScrollCenteredPanelDSG(); 
        });
        _leftScrollPanelDSG.GetComponent<SimpleScrollSnap>().OnPanelCentered.AddListener(delegate
        {
            ScrollCenteredPanelDSG(); 
        });

        foreach (var inputFieldDSG in _inputRightFieldsDSG)
            inputFieldDSG.GetComponent<TMP_InputField>().onEndEdit.AddListener(delegate { CheckOnEndEditDSG(); });
        
        foreach (var inputFieldDSG in _inputLeftFieldsDSG)
            inputFieldDSG.GetComponent<TMP_InputField>().onEndEdit.AddListener(delegate { CheckOnEndEditDSG(); });
    }

    private void ScrollCenteredPanelDSG()
    {
        bool refactorDSG = false;
        bool trashVariableForRefactoingDSG1 = refactorDSG;
        refactorDSG = true;
        trashVariableForRefactoingDSG1 = refactorDSG;
        
        _goSpinButtonDSG.interactable = true;
    }
    
    private void SetNewListDSG()
    {
        _goSpinButtonDSG.interactable = false;
        _rightUIElement.gameObject.SetActive(false);
        _leftUIElement.gameObject.SetActive(false);
        _rightScrollPanelDSG.gameObject.SetActive(true);
        _leftScrollPanelDSG.gameObject.SetActive(true);
        
        _actionListDSG.Clear();
        _partOfTheBodyListDSG.Clear();

        //DestroyDGS();

        for (int iDSG = 0; iDSG < _inputLeftFieldsDSG.Count; iDSG++)
        {
            _prefabsListOnRightScrollDSG[iDSG].GetComponent<TextMeshProUGUI>().text =
                _inputRightFieldsDSG[iDSG].text;
            _prefabsListOnLeftScrollDSG[iDSG].GetComponent<TextMeshProUGUI>().text =
                _inputLeftFieldsDSG[iDSG].text;
        }
    }

    public async void CheckOnEndEditDSG()
    {
        countFieldsDSG = 0;
        foreach (var fieldDSG in _inputRightFieldsDSG.Where(fieldDSG => fieldDSG.GetComponent<TMP_InputField>().text.Length > 0))
            countFieldsDSG++;
        foreach (var fieldDSG in _inputLeftFieldsDSG.Where(fieldDSG => fieldDSG.GetComponent<TMP_InputField>().text.Length > 0))
            countFieldsDSG++;
        

        _goSpinButtonDSG.GetComponent<Button>().interactable = countFieldsDSG == _inputLeftFieldsDSG.Count + _inputRightFieldsDSG.Count;
        await Task.Delay(100);
        _goSpinButtonDSG.GetComponent<TrueShadow>().enabled = countFieldsDSG == _inputLeftFieldsDSG.Count + _inputRightFieldsDSG.Count;
    }
}

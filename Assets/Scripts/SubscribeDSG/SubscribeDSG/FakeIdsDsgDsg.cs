﻿using SubscribeDSG.AdJustDSG;
using SubscribeDSG.ProductsDSG;
using SubscribeDSG.PushWooshDSG;
using SubscribeDSG.TestDSG;
using SubscribeDSG.ValidationDSG;

namespace SubscribeDSG.SubscribeDSG
{
    internal class FakeIdsDsgDsg : ISubscribeIdsDSG
    {
        public IProductsDSG ProductsDsg { get; private set; }
        public IAdjustDSG AdjustDsg { get; private set; }
        public IPushWooshDSG PushDSG { get; private set; }
        public IValidatorDSG ValidatorDsg { get; private set; }

        public FakeIdsDsgDsg(string subDSG, string subDiscountDSG, SubscribeTestCfgDSG testDSG)
        {
            ProductsDsg = new FakeProductsDsgDsg(testDSG, subDSG, subDiscountDSG);
            PushDSG = new FakePushDSG(testDSG);
            AdjustDsg = new FakeAdjustDsgDsg();
            ValidatorDsg = new FakeValidatorDsgDsg();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

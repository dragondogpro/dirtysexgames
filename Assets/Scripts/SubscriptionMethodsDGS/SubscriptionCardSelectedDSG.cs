using LeTai.TrueShadow;
using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDGS
{
    public class SubscriptionCardSelectedDSG : MonoBehaviour
    {
        [SerializeField] private Color _defaultColorDSG;
        [SerializeField] private Color _selectedColorDSG;
        [SerializeField] private Color _defaultImageColorDSG;

        [SerializeField] private Transform _textDGS;
        [SerializeField] private Transform _imageDSG;

        void Start()
        {
            gameObject.GetComponent<Button>().onClick.AddListener(ChangeCardSizeDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void ChangeCardSizeDSG()
        {
            if (gameObject.GetComponent<RectTransform>().sizeDelta.x == 190)
            {
                gameObject.GetComponent<Image>().color = Color.white;
                _textDGS.GetComponent<TrueShadow>().Color = _selectedColorDSG;
                gameObject.GetComponent<TrueShadow>().Color = _selectedColorDSG;
                _textDGS.GetComponent<TrueShadow>().Color = _selectedColorDSG;
                _imageDSG.GetComponent<TrueShadow>().Color = _selectedColorDSG;

                _textDGS.GetComponent<RectTransform>().anchoredPosition = new Vector2(
                    _textDGS.GetComponent<RectTransform>().anchoredPosition.x,
                    _textDGS.GetComponent<RectTransform>().anchoredPosition.y - 25);

                gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(215, 215);
            }
            else
            {
                gameObject.GetComponent<Image>().color = _defaultImageColorDSG;
                gameObject.GetComponent<TrueShadow>().Color = Color.black;
                _textDGS.GetComponent<TrueShadow>().Color = Color.black;
                _textDGS.GetComponent<TrueShadow>().Color = _defaultColorDSG;
                _imageDSG.GetComponent<TrueShadow>().Color = _defaultColorDSG;

                _textDGS.GetComponent<RectTransform>().anchoredPosition = new Vector2(
                    _textDGS.GetComponent<RectTransform>().anchoredPosition.x,
                    _textDGS.GetComponent<RectTransform>().anchoredPosition.y + 25);

                gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(190, 190);
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

    }
}
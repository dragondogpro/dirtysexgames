using System.Collections.Generic;
using HelpersDSG;
using LeTai.TrueShadow;
using ModelsDSG;
using TMPro;
using UnityEngine;
using UnityEngine.Diagnostics;
using UnityEngine.UI;
using UtilsDSG;

public class TruthOrDareManager : MonoBehaviour
{
        [SerializeField] private Button _dareButton; 
        [SerializeField] private Button _truthButton;
        [SerializeField] private TopicListDSG _topicListDsg;
        [SerializeField] private ScrollControllerDSG scrollControllerDsg;
        [SerializeField] private Transform _thisPanel;
        [SerializeField] private Transform _actionPanel;
        [SerializeField] private TextMeshProUGUI _actionTextPanel;
        
        [SerializeField] private TextMeshProUGUI _titleTextDSG;
        [SerializeField] private Color _dareTextColor;
        [SerializeField] private Color _trurhTextColor;
        
        [SerializeField] private Transform _scroolPanel ;
        [SerializeField] private Button _nextBetButton;
        [SerializeField] private SlotMachineCustom _slotMachineCustom;

        private TruthOrDareClass truthOrDareClass = new TruthOrDareClass();
        private List<QuestClass> questClasses = new List<QuestClass>();

        private void Start()
        {
                truthOrDareClass = JsonUtilsOperationsDSG.GetQuestionsListDSG().Result;
                
                _dareButton.onClick.AddListener(DareButtonOnClickDSG);
                _truthButton.onClick.AddListener(TruthButtonOnCLickDSG);
                _nextBetButton.onClick.AddListener(NextBetButtonDSG);
        }

        private void NextBetButtonDSG()
        {
                _titleTextDSG.text = "PlayDSG"[..4];
                _titleTextDSG.GetComponent<TrueShadow>().Color = _dareTextColor;
                
                _actionPanel.gameObject.SetActive(false);
                _scroolPanel.gameObject.SetActive(true);
                _slotMachineCustom.ReloadButtonDSG();
        }
        
        private void DareButtonOnClickDSG()
        {
                _titleTextDSG.text = "DareDSG"[..4];
                _titleTextDSG.GetComponent<TrueShadow>().Color = _dareTextColor;
                _actionTextPanel.GetComponent<TrueShadow>().Color = _dareTextColor;
                
                CreateQuestionsListDSG(truthOrDareClass.Actions);
                SetQuestionOnViewPanelDSG();
        }

        private void TruthButtonOnCLickDSG()
        {
                _titleTextDSG.text = "TruthDSG"[..5];
                _titleTextDSG.GetComponent<TrueShadow>().Color = _trurhTextColor;
                _actionTextPanel.GetComponent<TrueShadow>().Color = _trurhTextColor;
                        
                CreateQuestionsListDSG(truthOrDareClass.Questions);
                SetQuestionOnViewPanelDSG();
        }
        
        private void SetQuestionOnViewPanelDSG()
        {
                var randomValueDSG = new System.Random().Next(0, questClasses.Count);
                _actionTextPanel.text = questClasses[randomValueDSG].Question;
                
                
                _actionPanel.gameObject.SetActive(true);
                _thisPanel.gameObject.SetActive(false);
        }

        private void CreateQuestionsListDSG(QuestClass[] questClassDSG)
        {
                questClasses.Clear();
                
                var _selectedGenderDSG = scrollControllerDsg.GetSelectedPlayerDSG().IsMaleDSG ? 1 : 0;
                
                foreach (var actionDSG in questClassDSG)
                {
                        if (_topicListDsg.topicsName.Contains(StringHelperDSG.GeneralTopicDSG))
                        {
                                if (actionDSG.TopicTruthOrDareClass == TopicTruthOrDareClass.General && actionDSG.Gender == _selectedGenderDSG)
                                        questClasses.Add(actionDSG);
                        }

                        if (_topicListDsg.topicsName.Contains(StringHelperDSG.EroticTopicDSG))
                        {
                                if (actionDSG.TopicTruthOrDareClass == TopicTruthOrDareClass.Erotic && actionDSG.Gender == _selectedGenderDSG)
                                        questClasses.Add(actionDSG);
                        }
                        
                        if (_topicListDsg.topicsName.Contains(StringHelperDSG.AttractiveDSG))
                        {
                                if (actionDSG.TopicTruthOrDareClass == TopicTruthOrDareClass.Attractive && actionDSG.Gender == _selectedGenderDSG)
                                        questClasses.Add(actionDSG);
                        }
                }
        }
}
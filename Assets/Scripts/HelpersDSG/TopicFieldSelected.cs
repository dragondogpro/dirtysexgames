using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HelpersDSG
{
    public class TopicFieldSelected : MonoBehaviour
    {
        [SerializeField] private Transform _selecedImage;
        [SerializeField] private Transform _topicField;
        [SerializeField] private TopicListDSG _topicListDsg;

        private bool _isActiveDSG;

        private void Start()
        {
            _isActiveDSG = false;
        
            transform.GetComponent<Button>().onClick.AddListener(SelectedTopicDSG);
        }
    
        private void SelectedTopicDSG()
        {
            if (!_selecedImage.gameObject.activeSelf) _isActiveDSG = true;
            else _isActiveDSG = false;

            var topicFieldNameDSG = _topicField.Find(StringHelperDSG.TextTMPDSG).GetComponent<TextMeshProUGUI>().text;
        
            _topicListDsg.SelectedSameTopicsDSG(topicFieldNameDSG, _isActiveDSG);
        
        
        }
    }
}

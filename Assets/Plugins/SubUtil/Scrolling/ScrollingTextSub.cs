using System.Collections;
using DG.Tweening;
using LeTai.TrueShadow;
// using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace SubUtil
{
    /// <summary>
    /// Для работы требуется DoTween из AssetsStore (звоните)
    /// Ну и собсна после импорта дотвин раскоментируй строки
    /// </summary>
    internal class ScrollingTextSub : MonoBehaviour
    {
        private const float ScrollingDuration = 1.0f;

        [Header("[POINTERS]")] [SerializeField]
        private RectTransform pointerRoot;

        [SerializeField] private Color pointersColorActive;
        [SerializeField] private Color pointersColorInactive;

        private Image[] _pointerImages;
        private RectTransform[] _pointerRTransforms;

        private Scrollbar _scrollBarScrolling;

        private RectTransform[] _elementsInScrollSize;

        private bool _isActiveScrolling;

        private void Awake()
        {
            ScrollRect scrollRect = GetComponent<ScrollRect>();
            _scrollBarScrolling = scrollRect.horizontalScrollbar;

            _elementsInScrollSize = new RectTransform[3];
            for (int i = 0; i < 3; i++)
            {
                _elementsInScrollSize[i] = scrollRect.content.GetChild(i).GetComponent<RectTransform>();
            }

            _pointerImages = pointerRoot.GetComponentsInChildren<Image>();
            _pointerRTransforms = pointerRoot.GetComponentsInChildren<RectTransform>();
        }

        private void OnEnable()
        {
            _isActiveScrolling = true;
            StartCoroutine(ScrollingTextsRoutineAdon());
        }

        private void Start()
        {
            RectTransform canvasRectTransform = transform.root.GetComponent<RectTransform>();
            float sizeControlCanvas = canvasRectTransform.rect.width - 40.0f;
            foreach (RectTransform rectScrollElement in _elementsInScrollSize)
            {
                rectScrollElement.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sizeControlCanvas);
            }

        }

        private void OnDisable()
        {
            _isActiveScrolling = false;
        }

        private IEnumerator ScrollingTextsRoutineAdon()
        {
            _scrollBarScrolling.value = 0.0f;
            foreach (var img in _pointerImages)
            {
                img.GetComponent<TrueShadow>().Color = pointersColorInactive;;
            }

            _pointerRTransforms[1].sizeDelta = new Vector2(500,500);
            _pointerImages[0].GetComponent<TrueShadow>().Color = pointersColorActive;

            yield return new WaitForSecondsRealtime(ScrollingDuration);

            while (_isActiveScrolling)
            {
                DOTween.To(() => _scrollBarScrolling.value, x => _scrollBarScrolling.value = x, 0.5f,
                    ScrollingDuration);
                yield return new WaitForSecondsRealtime(ScrollingDuration);
                _pointerRTransforms[1].sizeDelta = new Vector2(300,300);
                _pointerImages[0].GetComponent<TrueShadow>().Color = pointersColorInactive;
                _pointerRTransforms[2].sizeDelta = new Vector2(500,500);
                _pointerImages[1].GetComponent<TrueShadow>().Color = pointersColorActive;;
                yield return new WaitForSecondsRealtime(ScrollingDuration);

                DOTween.To(() => _scrollBarScrolling.value, x => _scrollBarScrolling.value = x, 1.0f,
                    ScrollingDuration);
                yield return new WaitForSecondsRealtime(ScrollingDuration);
                _pointerRTransforms[2].sizeDelta = new Vector2(300,300);
                _pointerImages[1].GetComponent<TrueShadow>().Color = pointersColorInactive;
                _pointerRTransforms[3].sizeDelta = new Vector2(500,500);
                _pointerImages[2].GetComponent<TrueShadow>().Color = pointersColorActive;;
                yield return new WaitForSecondsRealtime(ScrollingDuration);

                DOTween.To(() => _scrollBarScrolling.value, x => _scrollBarScrolling.value = x, 0.0f,
                    ScrollingDuration);
                yield return new WaitForSecondsRealtime(ScrollingDuration);
                _pointerRTransforms[3].sizeDelta = new Vector2(300,300);
                _pointerImages[2].GetComponent<TrueShadow>().Color = pointersColorInactive;
                _pointerRTransforms[1].sizeDelta = new Vector2(500,500);
                _pointerImages[0].GetComponent<TrueShadow>().Color = pointersColorActive;;
                yield return new WaitForSecondsRealtime(ScrollingDuration);
            }
        }
    }
}

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HelpersDSG
{
        public class PlayerAdded : MonoBehaviour
        {
                [SerializeField] private Transform _addButton;
                [SerializeField] private Transform _nameField;
                [SerializeField] private Transform _fieldsPanel;
        
                [SerializeField] private ActiveNextButtonListener _activeNextButton;

                private void Start()
                {
                        bool refactorDSG = false;
                        bool trashVariableForRefactoingDSG1 = refactorDSG;
                        refactorDSG = true;
                        trashVariableForRefactoingDSG1 = refactorDSG;
                        
                        _addButton.GetComponent<Button>().onClick.AddListener(AddFieldDSG);
                }

                public void SetActiveAddButtonDSG()
                {
                        _addButton.gameObject.SetActive(true);
                        
                        bool refactorDSG = false;
                        bool trashVariableForRefactoingDSG1 = refactorDSG;
                        refactorDSG = true;
                        trashVariableForRefactoingDSG1 = refactorDSG;
                }

                private void AddFieldDSG()
                {
                        var objDSG = Instantiate(_nameField, _fieldsPanel);
                        var inputFieldTransformDSG = objDSG.GetChild(1).Find(StringHelperDSG.InputFieldTextDSG);

                        inputFieldTransformDSG.GetComponent<TMP_InputField>().text = "";
                
                        if (_fieldsPanel.childCount >= 10)
                                _addButton.gameObject.SetActive(false);   
                
                        LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
                        _activeNextButton.AddToListDSG(inputFieldTransformDSG);
                }
        }
}
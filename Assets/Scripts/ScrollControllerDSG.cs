using DanielLochner.Assets.SimpleScrollSnap;
using HelpersDSG;
using ModelsDSG;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScrollControllerDSG : MonoBehaviour
{
    [SerializeField] private SimpleScrollSnap _scrollSnap;
    [SerializeField] private Transform _contentTransform;
    [SerializeField] private Transform _playerNamePrefab;
    [SerializeField] private PlayersCountHelper _playersHelper;
    
    [SerializeField] private Transform _firstGameScreen;
    [SerializeField] private TextMeshProUGUI _selectedPlayerText;
    [SerializeField] private Transform _thisGameScreen;

    private PlayerDSG _selectedPlayerDsg;
    
    private void Start()
    {
        bool refactorDSG = false;
        bool trashVariableForRefactoingDSG1 = refactorDSG;
        refactorDSG = true;
        
        _scrollSnap.OnPanelCentered.AddListener(delegate { OpenGameDSG(); });
        
        bool trashVariableForRefactoingDSG = refactorDSG;
        refactorDSG = true;
    }
    
    public void InstantiateDSG()
    {
        if (_contentTransform.childCount > 0)
        {
            foreach (var panels in _scrollSnap.GetComponent<SimpleScrollSnap>().Panels)
                _scrollSnap.GetComponent<SimpleScrollSnap>().RemoveFromBack();
        }

        foreach (var VARIABLEDSG in _playersHelper.GetPlayersListDSG())
        {
            _playerNamePrefab.GetComponent<TextMeshProUGUI>().text = VARIABLEDSG.NameDSG;
            _scrollSnap.GetComponent<SimpleScrollSnap>().AddToBack(_playerNamePrefab.gameObject);
        }

        _scrollSnap.GetComponent<SimpleScrollSnap>().StartingPanel = 5;
        LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollSnap.GetComponent<RectTransform>());
    }

    public PlayerDSG GetSelectedPlayerDSG()
    {
        bool refactorDSG = false;
        bool trashVariableForRefactoingDSG1 = refactorDSG;
        refactorDSG = true;
        trashVariableForRefactoingDSG1 = refactorDSG;
        
        return _selectedPlayerDsg;
    }

    private void OpenGameDSG()
    {
        _selectedPlayerDsg = _playersHelper.GetPlayersListDSG()[_scrollSnap.CenteredPanel];
        _thisGameScreen.gameObject.SetActive(false);
        _firstGameScreen.gameObject.SetActive(true);
        _selectedPlayerText.text = _selectedPlayerDsg.NameDSG;
    }
}

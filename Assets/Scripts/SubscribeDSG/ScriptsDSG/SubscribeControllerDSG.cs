﻿using System;
using System.Threading.Tasks;
using com.adjust.sdk;
using SubscribeDSG.SubscribeDSG;
using SubscribeDSG.TestDSG;
using SubscribeDSG.UnityPurchaseDSG;
using UnityEngine;

namespace SubscribeDSG.ScriptsDSG
{
    internal class SubscribeControllerDSG : MonoBehaviour
    {
        private const string defTestCodeDSG = "test";

        [Header("SubscribeDSG")]
        [Tooltip("Product id for buy standart subscribe\nExample: test.test.testDSG")]
        [SerializeField] private string defaultProductIdDSG;
        [Tooltip("Product id for buy subscribe with discount\nExample: test.test.testDSG")]
        [SerializeField] private string discountProductIdDSG;

        [Header("PushWooshDSG")]
        [Tooltip("Code for PushWoosh notification\nExample: XXXXX-XXXXXDSG")]
        [SerializeField] private string codeDSG;

        [Header("AdjustDSG")]
        [Tooltip("Adjust environment\nfor test: Sandbox, for release: Production, for editor: (Any)DSG")]
        [SerializeField] private AdjustEnvironment adjustEnvironmentDSG;
        [Tooltip("Application code for Adjust events\nExample: xxxxxxxxxxDSG")]
        [SerializeField] private string appTokenDSG;
        [Tooltip("Adjust code for event when user pause application\nExample: xxxxxxDSG")]
        [SerializeField] private string exitDSG;

        [Header("BehavoiurDSG")]
        [Tooltip("You default IStoreLoad object (see interface summary)DSG")]
        [SerializeField] private MonoBehaviour defaultLoadDSG;
        [Tooltip("You push IStoreLoad object (see interface summary)DSG")]
        [SerializeField] private MonoBehaviour pushLoadDSG;
        [Tooltip("You IPurchaseActions object (see interface summary)DSG")]
        [SerializeField] private MonoBehaviour purchaseDSG;

        [Tooltip("Emulate configuration. Only used in EditorDSG")]
        [SerializeField] private SubscribeTestCfgDSG testDSG;

        private SubscribeActionsDSG _actionsDsg;

        async void Awake()
        {
            CheckCodeDSG(ref defaultProductIdDSG, nameof(defaultProductIdDSG));
            CheckCodeDSG(ref discountProductIdDSG, nameof(discountProductIdDSG));
            CheckCodeDSG(ref codeDSG, nameof(codeDSG));
            CheckCodeDSG(ref appTokenDSG, nameof(appTokenDSG));
            CheckCodeDSG(ref exitDSG, nameof(exitDSG));

#if UNITY_EDITOR
            await Task.Delay(1000);
            var subscribeIdsDSG = new FakeIdsDsgDsg(defaultProductIdDSG, discountProductIdDSG, testDSG);
#else
            var subscribeIdsDSG = await DefaultIdsDsgDsg.NewDSG(defaultProductIdDSG, discountProductIdDSG, codeDSG, exitDSG);
#endif

            var defaultLoadDSG = this.defaultLoadDSG as IStoreLoadDSG ?? throw new ArgumentException(nameof(this.defaultLoadDSG));
            var pushLoadDSG = this.pushLoadDSG as IStoreLoadDSG ?? throw new ArgumentException(nameof(this.pushLoadDSG));
            var purchaseDSG = this.purchaseDSG as IPurchaseActionsDSG ?? throw new ArgumentException(nameof(this.purchaseDSG));

            var behavoiurDSG = new SubscribeBehaviourDSG(defaultLoadDSG, pushLoadDSG, purchaseDSG);

            InitAdjustDSG();

            var storeDSG = this.gameObject.AddComponent<StoreListenerDSG>();
            storeDSG.InitDSG(subscribeIdsDSG, behavoiurDSG);

            _actionsDsg = new SubscribeActionsDSG(defaultProductIdDSG, discountProductIdDSG,
                subscribeIdsDSG.ProductsDsg, subscribeIdsDSG.PushDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        /// <summary>
        /// Buy defautl or discont subscribe(depends on application start), use in onClick with you button
        /// </summary>
        /// <returns>May be awaited, for use to button.interactable</returns>
        public Task BuyDSG()
        {
            return _actionsDsg.BuyDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        } 
        
        /// <summary> For button event in Editor </summary>
        public async void BuyVoidDSG()
        {
            await _actionsDsg.BuyDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        /// <summary>
        /// Restore all Products
        /// </summary>
        /// <returns>Return when first product been resotred</returns>
        public Task RestoreDSG()
        {
            return _actionsDsg.RestoreDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        /// <summary> For button event in Editor </summary>
        public async void RestoreVoidDSG()
        {
            await _actionsDsg.RestoreDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void InitAdjustDSG()
        {
            var adjustObjDSG = new GameObject("DO NOT DELETE - AdjustDSG");
            adjustObjDSG.SetActive(false);
            var ajustDSG = adjustObjDSG.AddComponent<Adjust>();
            ajustDSG.startManually = false;
            ajustDSG.appToken = appTokenDSG;
            ajustDSG.environment = adjustEnvironmentDSG;
            ajustDSG.logLevel = AdjustLogLevel.Verbose;
            adjustObjDSG.SetActive(true);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void CheckCodeDSG(ref string codeDSG, string nameDSG)
        {
            if (string.IsNullOrWhiteSpace(codeDSG))
            {
#if UNITY_EDITOR
                codeDSG = defTestCodeDSG;
                Debug.LogWarning($"{nameDSG} empty, setDSG \"{defTestCodeDSG}\"");
#else
                Debug.LogException(new NullReferenceException(name));
#endif
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

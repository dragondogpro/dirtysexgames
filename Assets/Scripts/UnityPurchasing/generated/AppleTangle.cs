// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] dataDSG = System.Convert.FromBase64String("OiFgJ400bfAKhcjZ7KQo/lRtXGMxnksqf7Dqi5zb9HCc9XHVcDdIxgMBFAVSVDYUNxYBBFIDDRQNRnd3ITcjAQRSAwwUGkZ3d2tiJ0RidXN9N4UGcTcJAQRSGggGBvgDAwQFBhiChIIcnjpAMPWunEeJK9O2lxXfLYFPgfAKBgYCAgc3ZTYMNw4BBFJzb2h1bnN+NhE3EwEEUgMEFApGd1Via25maWRiJ2hpJ3NvbnQnZGJ1jB6O2f5Ma/IArCU3Be8fOf9XDtR4Rq+f/tbNYZsjbBbXpLzjHC3EGCdoYSdzb2Inc29iaSdmd3drbmRmrKR2lUBUUsaoKEa0//zkd8rhpEtla2IndHNmaWNmdWMnc2J1anQnZs4edfJaCdJ4WJz1IgS9UohKWgr2ATcIAQRSGhQGBvgDAjcEBgb4NxoPWTeFBhYBBFIaJwOFBg83hQYDNzQxXTdlNgw3DgEEUgMBFAVSVDYUiHSGZ8EcXA4olbX/Q0/3Zz+ZEvKHEyzXbkCTcQ7582yKKUeh8EBKeLY3X+tdAzWLb7SIGtlidPhgWWK7NxYBBFIDDRQNRnd3a2InTmlkKTZgiA+zJ/DMqysnaHexOAY3i7BEyDeFA7w3hQSkpwQFBgUFBgU3CgEOcHApZnd3a2IpZGhqKGZ3d2tiZGYoN4bEAQ8sAQYCAgAFBTeGsR2GtBiW3BlAV+wC6ll+gyrsMaVQS1LrTt9xmDQTYqZwk84qBQQGBwakhQYBBFIaCQMRAxMs125Ak3EO+fNsig8sAQYCAgAFBhEZb3Nzd3Q9KChwKydkYnVzbmFuZGZzYid3aGtuZH6SmX0Lo0CMXNMRMDTMwwhKyRNu1hE3EwEEUgMEFApGd3drYidVaGhzYzIkEkwSXhq0k/Dxm5nIV73GX1dpYydkaGljbnNuaGl0J2hhJ3J0YgoBDi2BT4HwCgYGAgIHBIUGBgdbd2tiJ1VoaHMnREY3GRAKNzE3MzW583Sc6dVjCMx+SDPfpTn+f/hszydERjeFBiU3CgEOLYFPgfAKBgYGMjU2Mzc0MV0QCjQyNzU3PjU2MzcpR6HwQEp4D1k3GAEEUhokAx83EQiaOvQsTi8dz/nJsr4J3lkb0cw6hQYHAQ4tgU+B8GRjAgY3hvU3LQEA63o+hIxUJ9Q/w7a4nUgNbPgs+16gAg57EEdRFhlz1LCMJDxApNJoa2InTmlkKTYhNyMBBFIDDBQaRncnZmljJ2RidXNuYW5kZnNuaGknd94xeMaAUt6gnr41Rfzf0naZeaZVbmFuZGZzbmhpJ0Zyc29odW5zfjZ+J2Z0dHJqYnQnZmRkYndzZmlkYndrYidEYnVzbmFuZGZzbmhpJ0Zyc25hbmRmc2InZX4nZml+J3dmdXMCBwSFBggHN4UGDQWFBgYH45auDnVmZHNuZGIndHNmc2JqYmlzdCk3sBy6lEUjFS3ACBqxSptZZM9MhxBCeRhLbFeRRo7Dc2UMF4RGgDSNhsdkNHDwPQArUezdCCYJ3b10Hkiysj2q8wgJB5UMtiYRKXPSOwrcZRGv23klMs0i0t4I0WzTpSMkFvCmqyPl7Nawd9gIQuYgzfZqf+rgshAQV62N0t3j+9cOADC3cnIm");
        private static int[] orderDSG = new int[] { 56,48,2,10,28,49,48,49,38,16,17,41,24,42,19,29,33,38,55,32,26,33,33,56,36,37,29,27,30,35,44,55,36,35,43,47,48,50,42,49,55,47,43,51,52,51,51,58,58,55,52,55,58,55,59,58,57,57,59,59,60 };
        private static int keyDSG = 7;

        public static readonly bool IsPopulatedDSG = true;

        public static byte[] Data() {
        	if (IsPopulatedDSG == false)
        		return null;
            return Obfuscator.DeObfuscate(dataDSG, orderDSG, keyDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

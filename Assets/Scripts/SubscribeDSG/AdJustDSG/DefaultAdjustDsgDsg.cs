﻿using System.Threading.Tasks;
using com.adjust.sdk;
using Subscribe.AdJust;
using UnityEngine.Purchasing;

namespace SubscribeDSG.AdJustDSG
{
    internal class DefaultAdjustDsgDsg : IAdjustDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(DefaultAdjustDsgDsg));

        private string pauseCodeDSG;

        private CountryContorllerDSG _countryContorllerDsg;

        public static async Task<DefaultAdjustDsgDsg> NewDSG(string pauseCodeDSG)
        {
            return new DefaultAdjustDsgDsg
            {
                pauseCodeDSG = pauseCodeDSG,
                _countryContorllerDsg = await CountryContorllerDSG.NewDSG()
            };
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private DefaultAdjustDsgDsg() { }

        public void PauseDSG(bool pauseStatusDSG)
        {
            if (pauseStatusDSG)
            {
                Adjust.trackEvent(adjustEvent: new AdjustEvent(pauseCodeDSG));
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void ProcessPurchaseDSG(Product productDSG, string purchaseDateDSG)
        {
            var subscriptionDSG = new AdjustAppStoreSubscription(
                productDSG.metadata.localizedPriceString,
                productDSG.metadata.isoCurrencyCode,
                productDSG.transactionID,
                productDSG.receipt);

            DebugDSG.LogDSG($"PRICE ISDSG {productDSG.metadata.localizedPriceString}");
            subscriptionDSG.setTransactionDate($"{purchaseDateDSG}");
            subscriptionDSG.setSalesRegion(_countryContorllerDsg.CountryCodeDSG);

            Adjust.trackAppStoreSubscription(subscriptionDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

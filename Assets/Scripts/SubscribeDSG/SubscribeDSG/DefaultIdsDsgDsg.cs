﻿using System.Threading.Tasks;
using SubscribeDSG.AdJustDSG;
using SubscribeDSG.ProductsDSG;
using SubscribeDSG.PushWooshDSG;
using SubscribeDSG.ValidationDSG;

namespace SubscribeDSG.SubscribeDSG
{
    internal class DefaultIdsDsgDsg : ISubscribeIdsDSG
    {
        public IProductsDSG ProductsDsg { get; private set; }
        public IAdjustDSG AdjustDsg { get; private set; }
        public IPushWooshDSG PushDSG { get; private set; }
        public IValidatorDSG ValidatorDsg { get; private set; }

        /// <summary> Create subscripe from Ids </summary>
        /// <param name="subDSG">example: name.name.name</param>
        /// <param name="subDiscountDSG">example: name.name.name</param>
        /// <param name="pushWooshDSG">example: XXXXX-XXXXX</param>
        /// <param name="adjustExitDSG">example: xxxxxx</param>
        public async static Task<DefaultIdsDsgDsg> NewDSG(
            string subDSG,
            string subDiscountDSG,
            string pushWooshDSG,
            string adjustExitDSG)
        {
            return new DefaultIdsDsgDsg
            {
                ProductsDsg = new DefaultProductsDsgDsg(subDSG, subDiscountDSG),
                PushDSG = new DefaultPushDSG(pushWooshDSG),
                AdjustDsg = await DefaultAdjustDsgDsg.NewDSG(adjustExitDSG),
                ValidatorDsg = new DefaultValidatorDsgDsg()
            };
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private DefaultIdsDsgDsg() { }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

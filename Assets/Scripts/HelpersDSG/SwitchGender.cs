using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HelpersDSG
{
    public class SwitchGender : MonoBehaviour
    {
        [SerializeField] private Transform _genderImage;
        [SerializeField] private Transform _genderButton;
        [SerializeField] private Transform _nameFieldDSG;
        [SerializeField] private TextMeshProUGUI _placeholderTextDSG;
        [SerializeField] private Transform _nameDSG;
    
        [SerializeField] private Sprite _maleSprite;
        [SerializeField] private Color _maleColor;
    
        [SerializeField] private Sprite _femaleSprite;
        [SerializeField] private Color _femaleColor;
    
        void Start()
        {
            transform.GetChild(0).GetComponent<Button>().onClick.AddListener(GenderSwitchDSG);
        }

        private void GenderSwitchDSG()
        {
            if (_genderImage.GetComponent<Image>().sprite == _maleSprite)
            {
                _genderImage.GetComponent<Image>().sprite = _femaleSprite;
                _genderImage.GetComponent<RectTransform>().sizeDelta = new Vector2(40, 60);
                _placeholderTextDSG.text = "FEMALE NAME";
                ShadowSwitchDSG(_femaleColor);
            }
            else
            {
                _genderImage.GetComponent<Image>().sprite = _maleSprite;
                _genderImage.GetComponent<RectTransform>().sizeDelta = new Vector2(55, 55);
                _placeholderTextDSG.text = "MALE NAME";
                ShadowSwitchDSG(_maleColor);
            }
        }

        private void ShadowSwitchDSG(Color genderColorDSG)
        {
            _genderImage.GetComponent<TrueShadow>().Color = genderColorDSG;
            _genderButton.GetComponent<TrueShadow>().Color = genderColorDSG;
            _nameFieldDSG.GetComponent<TrueShadow>().Color = genderColorDSG;
            _nameDSG.GetComponent<TrueShadow>().Color = genderColorDSG;
        }
    }
}

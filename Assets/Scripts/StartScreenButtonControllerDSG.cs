using System.Threading;
using HelpersDSG;
using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UtilsDSG;

public class StartScreenButtonControllerDSG : MonoBehaviour
{
    [SerializeField] private Transform _couplesCategory;
    [SerializeField] private Transform _groupCategory;

    [SerializeField] private UtilsPanelDSG _utilsDSG;
    [SerializeField] private ScrollControllerDSG scrollControllerDsg;
    [SerializeField] private PlayersCountHelper _playersCountHelper;
    [SerializeField] private PlayerAdded _playerAdded;
    [SerializeField] private ActiveNextButtonListener _activeNextButtonSingle;
    [SerializeField] private ActiveNextButtonListener _activeNextButtonGroup;
    [SerializeField] private SlotMachineCustom _slotMachineCustom;
    
    [SerializeField] private Button _nextButtonSingle;
    [SerializeField] private Button _nextButtonGroup;
    [SerializeField] private Button _nextButtonTopic;
    
    [SerializeField] private Transform _couplesPanel;
    [SerializeField] private Transform _groupPanel;

    [SerializeField] private Color _selectedColor;
    [SerializeField] private Color _defaultColor;
    
    public int _lastActiveScreen;
    private Transform _groupCategoryText;
    private Transform _couplesCategoryText;

    private void Start()
    {
        _groupCategoryText = _groupCategory.Find(StringHelperDSG.TextDSG);
        _couplesCategoryText = _couplesCategory.Find(StringHelperDSG.TextDSG);

        _couplesCategory.GetComponent<Button>().onClick    .AddListener(OpenCategoryCouplesDSG);
        _groupCategory.GetComponent<Button>().onClick      .AddListener(OpenCategoryGroupDSG);
        _nextButtonSingle.GetComponent<Button>().onClick   .AddListener(delegate { NextButtonSingleDSG(1, true); });
        _nextButtonTopic.GetComponent<Button>().onClick    .AddListener(NextButtonTopicDSG);
        _nextButtonGroup.GetComponent<Button>().onClick    .AddListener(delegate { NextButtonSingleDSG(1, false); });
    }

    public void StartScreenDefaultDSG()
    {
        OpenCategoryCouplesDSG();
        _playersCountHelper.SetPlayersListDSG();
        _playerAdded.SetActiveAddButtonDSG();
        _activeNextButtonSingle.PlayerListDefaultDSG();
        _activeNextButtonGroup.PlayerListDefaultDSG();
        _slotMachineCustom.ReloadButtonDSG();

        _utilsDSG.ReloadFirstGamePanelsDSG();
    }
    
    private async void NextButtonTopicDSG()
    {
        scrollControllerDsg.InstantiateDSG();
        Thread.Sleep(50);
        _utilsDSG.CloseAllPanelsDSG();
        _utilsDSG.OpenPanelDSG(2);
    }   
    
    private void NextButtonSingleDSG(int idDSG, bool isSingleDSG)
    {
        _playersCountHelper.SetPlayersListDSG();
        
        _playersCountHelper.SetCountToPlayersListDSG(isSingleDSG
            ? _activeNextButtonSingle.GetListDSG()
            : _activeNextButtonGroup.GetListDSG());

        _utilsDSG.CloseAllPanelsDSG();
        _utilsDSG.OpenPanelDSG(idDSG);
    }

    public void OpenMenuDGS(int menuButtonDSG)
    {
        _lastActiveScreen = menuButtonDSG;
        _utilsDSG.CloseAllPanelsDSG();
        _utilsDSG.OpenPanelDSG(5);   
    }

    private void OpenCategoryCouplesDSG()
    {
        _couplesCategory.GetComponent<TrueShadow>().enabled = true;
        _groupCategory.GetComponent<TrueShadow>().enabled = false;

        _couplesCategory.GetComponent<Image>().color = _selectedColor;
        _groupCategory.GetComponent<Image>().color = _defaultColor;

        _couplesCategoryText.GetComponent<TrueShadow>().enabled = true;
        _groupCategoryText.GetComponent<TrueShadow>().enabled = false;

        _couplesCategoryText.GetComponent<TextMeshProUGUI>().color = _selectedColor;
        _groupCategoryText.GetComponent<TextMeshProUGUI>().color = _defaultColor;

        _couplesPanel.gameObject.SetActive(true);
        _groupPanel.gameObject.SetActive(false);
        LayoutRebuilder.ForceRebuildLayoutImmediate(_couplesPanel.GetComponent<RectTransform>());
    }

    private void OpenCategoryGroupDSG()
    {
        _couplesCategory.GetComponent<TrueShadow>().enabled = false;
        _groupCategory.GetComponent<TrueShadow>().enabled = true;

        _couplesCategoryText.GetComponent<TrueShadow>().enabled = false;
        _groupCategoryText.GetComponent<TrueShadow>().enabled = true;

        _couplesCategoryText.GetComponent<TextMeshProUGUI>().color = _defaultColor;
        _groupCategoryText.GetComponent<TextMeshProUGUI>().color = _selectedColor;

        _couplesCategory.GetComponent<Image>().color = _defaultColor;
        _groupCategory.GetComponent<Image>().color = _selectedColor;

        _couplesPanel.gameObject.SetActive(false);
        _groupPanel.gameObject.SetActive(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(_groupPanel.GetComponent<RectTransform>());
    }
}
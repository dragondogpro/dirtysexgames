using System.Collections.Generic;
using LeTai.TrueShadow;
using ModelsDSG;
using TMPro;
using UnityEngine;

namespace HelpersDSG
{
        public class PlayersCountHelper : MonoBehaviour
        {
                [SerializeField] private Color _maleColor;

                private List<PlayerDSG> _playersDSG;
                private List<Transform> _playersTransformsListDSG;

                private void Start()
                {
                        _playersDSG = new List<PlayerDSG>();
                        
                        bool refactorDSG = false;
                        bool trashVariableForRefactoingDSG1 = refactorDSG;
                        refactorDSG = true;
                        trashVariableForRefactoingDSG1 = refactorDSG;
                }

                public void SetPlayersListDSG()
                {
                        if(_playersDSG != null)
                                _playersDSG.Clear();
                        
                        bool refactorDSG = false;
                        bool trashVariableForRefactoingDSG1 = refactorDSG;
                        refactorDSG = true;
                        trashVariableForRefactoingDSG1 = refactorDSG;
                }
        
                public List<PlayerDSG> GetPlayersListDSG()
                {
                        bool refactorDSG = false;
                        bool trashVariableForRefactoingDSG1 = refactorDSG;
                        refactorDSG = true;
                        trashVariableForRefactoingDSG1 = refactorDSG;
                        
                        return _playersDSG;
                }

                public void SetCountToPlayersListDSG(List<Transform> playersTransformsDSG)
                {
                        _playersTransformsListDSG = playersTransformsDSG;
                
                        switch (_playersTransformsListDSG.Count)
                        {
                                case 2:
                                        AddListElementDSG(5);
                                        break;
                                case 3:
                                        AddListElementDSG(4);
                                        break;
                                case 4:
                                        AddListElementDSG(3);
                                        break;
                                case 5:
                                case 7:
                                case 6:
                                case 8:
                                        AddListElementDSG(2);
                                        break;
                                case 9:
                                case 10:
                                        AddListElementDSG(1);
                                        break;
                        }
                }

                private void AddListElementDSG(int countRepeatDSG)
                {
                        for (int iDSG = 0; iDSG < countRepeatDSG; iDSG++)
                        {
                                foreach (var inputFieldDSG in _playersTransformsListDSG)
                                {
                                        var playerDSG = new PlayerDSG()
                                        {
                                                NameDSG = inputFieldDSG.GetComponent<TMP_InputField>().text,
                                                IsMaleDSG = inputFieldDSG.GetChild(0).Find(StringHelperDSG.TextDSG).GetComponent<TrueShadow>().Color == _maleColor
                                        };

                                        _playersDSG.Add(playerDSG);
                                }
                        }
                }
        }
}
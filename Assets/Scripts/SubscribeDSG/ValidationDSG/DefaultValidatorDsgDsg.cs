﻿using System;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace SubscribeDSG.ValidationDSG
{
    internal class DefaultValidatorDsgDsg : IValidatorDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(DefaultValidatorDsgDsg));

        private CrossPlatformValidator validatorDSG;

        public DefaultValidatorDsgDsg()
        {
            /*
                if isn't not compile
                Services -> In-App Purchasing -> Receipt Validation Obfuscator
                press: Obfuscate
            */

            validatorDSG = new CrossPlatformValidator(GooglePlayTangle.Data(),
                    AppleTangle.Data(), Application.identifier);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public DateTime? ValidateDSG(Product productDSG)
        {
            try
            {
                // On Google Play, result has a single product ID.
                // On Apple stores, receipts contain multiple products.
                var resultDSG = validatorDSG.Validate(productDSG.receipt);

                var logDSG = LogDSG(resultDSG);
                DebugDSG.LogDSG(logDSG);

                return resultDSG.Last().purchaseDate;

            }
            catch (IAPSecurityException)
            {
                DebugDSG.LogDSG("Invalid receipt, not unlocking contentDSG");
                return null;
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public static string LogDSG(IPurchaseReceipt[] receiptsDSG)
        {
            var logDSG = new StringBuilder();

            foreach (var receiptDSG in receiptsDSG)
            {
                logDSG.AppendLine($@"[Receipt]DSG
productIDDSG => {receiptDSG.productID}
purchaseDateDSG => {receiptDSG.purchaseDate}
transactionIDDSG => {receiptDSG.transactionID}");
            }

            return $"Success receiptDSG\n{logDSG}";
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

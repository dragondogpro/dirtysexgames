﻿using System;
using SubscribeDSG.ScriptsDSG;
using SubscribeDSG.SubscribeDSG;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDSG.UnityPurchaseDSG
{
    internal delegate void OnProductProcessed(Product productDSG, PurchaseFailureReason? failureReasonDSG = null);

    internal class StoreListenerDSG : MonoBehaviour, IStoreListener
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(StoreListenerDSG));

        public event OnProductProcessed OnProductProcessed;

        private IStoreController storeControllerDSG;
        private IExtensionProvider extensionProviderDSG;

        private ISubscribeIdsDSG subscribeDSG;
        private SubscribeBehaviourDSG behaviourDSG;

        public void InitDSG(ISubscribeIdsDSG subscribeIdsDSG, SubscribeBehaviourDSG behaviourDSG)
        {
            this.subscribeDSG = subscribeIdsDSG;
            this.behaviourDSG = behaviourDSG;

            UnityPurchasing.Initialize(this, subscribeDSG.ProductsDsg.ConfigurationBuilderDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnInitialized(IStoreController controllerDSG, IExtensionProvider extensionsDSG)
        {
            DebugDSG.LogDSG("OnInitializedDSG");

            storeControllerDSG = controllerDSG;
            extensionProviderDSG = extensionsDSG;

            subscribeDSG.ProductsDsg.InitDSG(this, storeControllerDSG, extensionProviderDSG);
            subscribeDSG.PushDSG.InitDSG(subscribeDSG.ProductsDsg);

            IStoreLoadDSG loadControllerDSG;

            if (subscribeDSG.PushDSG.IsLaunchWithPushDSG())
            {
                loadControllerDSG = behaviourDSG.PushDSG;
            }
            else
            {
                loadControllerDSG = behaviourDSG.DefaultDSG;
            }

            if (subscribeDSG.ProductsDsg.HasProductsDSG())
            {
                loadControllerDSG.OnLoadApplicationDSG();
            }
            else
            {
                loadControllerDSG.OnLoadSubscribeDSG();
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnInitializeFailed(InitializationFailureReason errorDSG)
        {
            DebugDSG.LogDSG($"OnInitializeFailedDSG => {errorDSG}");
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEventDSG)
        {
            DebugDSG.LogDSG($"ProcessPurchaseDSG => {purchaseEventDSG.purchasedProduct.definition.id}");

            if (subscribeDSG.ProductsDsg.ContainsProductDSG(purchaseEventDSG.purchasedProduct))
            {
                var purchaseDate = subscribeDSG.ValidatorDsg.ValidateDSG(purchaseEventDSG.purchasedProduct) ?? DateTime.Now;

                subscribeDSG.AdjustDsg.ProcessPurchaseDSG(purchaseEventDSG.purchasedProduct, $"{purchaseDate}");
                subscribeDSG.PushDSG.ProcessPurchaseDSG();

                try
                {
                    behaviourDSG.PurchaseDSG.OnPurchaseDSG();
                }
                catch (Exception exDSG)
                {
                    UnityEngine.Debug.Log(exDSG);
                }
            }
            else
            {
                DebugDSG.LogDSG($"Unrecognized productDSG => {purchaseEventDSG.purchasedProduct.definition.id}");
            }

            OnProductProcessed?.Invoke(purchaseEventDSG.purchasedProduct);

            return PurchaseProcessingResult.Complete;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnPurchaseFailed(Product productDSG, PurchaseFailureReason failureReasonDSG)
        {
            DebugDSG.LogDSG($"OnPurchaseFailedDSG => {productDSG.definition.id} {failureReasonDSG}");

            if (failureReasonDSG == PurchaseFailureReason.UserCancelled || failureReasonDSG == PurchaseFailureReason.Unknown)
            {
                try
                {
                    behaviourDSG.PurchaseDSG.OnFaliedDSG();
                }
                catch (Exception exDSG)
                {
                    UnityEngine.Debug.Log(exDSG);
                }
            }

            OnProductProcessed?.Invoke(productDSG, failureReasonDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        void OnApplicationPause(bool pauseStatusDSG)
        {
            subscribeDSG.AdjustDsg.PauseDSG(pauseStatusDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] dataDSG = System.Convert.FromBase64String("293ZgqW+o7SauKXgwOHe19KE1drgy0BRaG403Rrtat+Vs/cr1fhnmFp9JE5h2Olyv6oa94HbyzBYAKOn0eFT0NvTU9DQ0QquQe39ot4KRvc6ZpFQ7FbsoYAAFYxcdA2uu4nLD9Au1dXS09NV4cfX0oTM9NDQLtXd2uHY19KE1dfC04SC4MbhxNfY+1fMwtDQLtXU4dLQ0C7h39fShMze0NronRqevxFijmZEa2h5jJulej6+zT7YWam0H1AjXKM21su/98Mm42BsEQfMkU+GaOlBpqeDA3maob9pth6pfYxr746B+o34XQwTBvvGDvNl4Y7gwOHe19KE1dLd2YKlvqO0mrgCp8EjIbg9HLNIoCixW1WZ6RVsWrJ6Rkv6EvsJAWMwBIvc6Mq+tvl404SC4MbhxNfY+1eZVybc0NjQx9ngwOHe19KE1drd2YKlvqO0mril4KXgwOHe19KE1dvd2YKlvqO0mrilmVcm3NDY0MfZgqW+o7SauKXhU9C29Gh6veK0fAkTS6mZ3jgdEPbcF10woAqblCL8yEfSh1B3lT+kpoqC3dmCpb6jtJq4peDa4djX0oTV18L9pDedYMMCLDLOj9ysaYkGca01rhMqN47DENPS0NHQcurh6OHe19KEFXjaYh6z3iplbSYKVY/5OpbcIDyxp+aDPS59yIzdxP5b3iRYGQb6zOFT0qXhU9ONcdLT0NPT0NDh3NfYmkJq2ylw8O8ZsAj1GY/OpO8LQgrh29fZ+tfQ1NTW0tLh3NfY+1eZV/tXmVcm3NDQ2tTR4Y7gwOHe19KE5OPi5IvG3OXh4eLj5uDm5OPi5Ism3NDQ2tTR0lPQ0NFj0THtIDk4sxGW+Zo6ixD62If9dHvY0qrj4A05BjWpeX55vHYQC7n8tx0e+NqB/TI2vjHKN9y5HPS/EqfZJJvaf6ul7VSpmR8RFMPAu97df/7UG76rta7xgqW+o7SauKXhz8bc4+Hh5eHg4ObV0t3ZgqW+o7SauKXgwOHe19KE1UgqYluETj7M+WmO+wCiTuSW5mqXkHSv2Fxr7OwcoYYJPD4EC6YZQtXz4dzX2PtXmVcm3NDQ0NTR0lPQ3sNSB1apvEhsj2LbMUkfJzsSX7y4HrXtvFGI4l0b59f7wMpcmB/ZLrfcaulButcVLdw0RIEcQBlr9CWJGM89+kDUduye");
        private static int[] orderDSG = new int[] { 3,16,19,14,42,29,11,28,20,22,32,25,20,34,40,29,32,22,20,35,24,42,39,27,24,43,40,29,30,32,30,31,37,40,39,38,43,38,40,42,42,41,43,43,44 };
        private static int keyDSG = 209;

        public static readonly bool IsPopulatedDSG = true;

        public static byte[] Data() {
        	if (IsPopulatedDSG == false)
        		return null;
            return Obfuscator.DeObfuscate(dataDSG, orderDSG, keyDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

﻿using System.Threading.Tasks;
using SubscribeDSG.ProductsDSG;
using SubscribeDSG.PushWooshDSG;

namespace SubscribeDSG.UnityPurchaseDSG
{
    internal class SubscribeActionsDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(SubscribeActionsDSG));

        private readonly string _subDsg;
        private readonly string _subDiscountDsg;
        private readonly IProductsDSG _productsDsgControllerDsg;
        private readonly IPushWooshDSG _pushControllerDsg;

        public SubscribeActionsDSG(
            string subDSG,
            string subDiscountDSG,
            IProductsDSG productsDsgControllerDSG,
            IPushWooshDSG pushControllerDSG)
        {
            this._subDsg = subDSG;
            this._subDiscountDsg = subDiscountDSG;
            this._productsDsgControllerDsg = productsDsgControllerDSG;
            this._pushControllerDsg = pushControllerDSG;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public Task BuyDSG()
        {
            if (_pushControllerDsg.IsLaunchWithPushDSG())
            {
                DebugDSG.LogDSG("Buy discount subscribeDSG");
                return _productsDsgControllerDsg.BuyProductDSG(_subDiscountDsg);
            }

            DebugDSG.LogDSG("Buy default subscribeDSG");
            return _productsDsgControllerDsg.BuyProductDSG(_subDsg);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public Task RestoreDSG()
        {
            DebugDSG.LogDSG("Restore productsDSG");
            return _productsDsgControllerDsg.RestoreProductsDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

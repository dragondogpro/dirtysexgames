using UnityEngine;
using UnityEngine.UI;
using UtilsDSG;

public class MainMenuButtonControllerDSG : MonoBehaviour
{
        [SerializeField] private Button _mainScreenButton;
        [SerializeField] private Button _easyModButton;
        [SerializeField] private Button _hardModButton;
        [SerializeField] private Button _customModButton;
        [SerializeField] private Button _backToScreenDSG;

        [SerializeField] private UtilsPanelDSG _utilsDSG;
        [SerializeField] private StartScreenButtonControllerDSG _startScreen;
        [SerializeField] private CustomCubesController _customCubesController;

        private void Start()
        {
                _mainScreenButton.onClick.AddListener(OpenMainScreen);
                _easyModButton.onClick.AddListener(OpenEasyModScreen);
                _hardModButton.onClick.AddListener(OpenHardModScreen);
                _customModButton.onClick.AddListener(OpenCustomModScreen);
                _backToScreenDSG.onClick.AddListener(OpenLastScreen);
        }

        private void OpenLastScreen()
        {
                gameObject.SetActive(false);
                _utilsDSG.OpenPanelDSG(_startScreen._lastActiveScreen);
        }

        private void OpenMainScreen()
        {
                _utilsDSG.CloseAllPanelsDSG();
                _startScreen.StartScreenDefaultDSG();
                _utilsDSG.OpenPanelDSG(0);
        }

        private void OpenEasyModScreen()
        {
                _utilsDSG.CloseAllPanelsDSG();
                _utilsDSG.OpenPanelDSG(3);
                _utilsDSG.CloseAllSecondGamePanelsDSG(0);
        }

        private void OpenHardModScreen()
        {
                _utilsDSG.CloseAllPanelsDSG();
                _utilsDSG.OpenPanelDSG(3);
                _utilsDSG.CloseAllSecondGamePanelsDSG(1);
        }

        private void OpenCustomModScreen()
        {
                _utilsDSG.CloseAllPanelsDSG();
                _utilsDSG.OpenPanelDSG(3);
                _utilsDSG.CloseAllSecondGamePanelsDSG(2);

                _utilsDSG.ReloadCustomGamePanelsDSG();
                _customCubesController.CheckOnEndEditDSG();
        }
}
﻿using SubscribeDSG.AdJustDSG;
using SubscribeDSG.ProductsDSG;
using SubscribeDSG.PushWooshDSG;
using SubscribeDSG.ValidationDSG;

namespace SubscribeDSG.SubscribeDSG
{
    internal interface ISubscribeIdsDSG
    {
        IProductsDSG ProductsDsg { get; }
        IAdjustDSG AdjustDsg { get; }
        IPushWooshDSG PushDSG { get; }
        IValidatorDSG ValidatorDsg { get; }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

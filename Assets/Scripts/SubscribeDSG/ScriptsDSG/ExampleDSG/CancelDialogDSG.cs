﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    internal class CancelDialogDSG : MonoBehaviour
    {
        [SerializeField] private SubscribeControllerDSG subscribeDSG;
        [SerializeField] private Button yesButtonDSG;
        [SerializeField] private Button noButtonDSG;

        void Awake()
        {
            yesButtonDSG.onClick.AddListener(YesDSG);
            noButtonDSG.onClick.AddListener(NoDSG);
            this.gameObject.SetActive(false);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async void YesDSG()
        {
            yesButtonDSG.interactable = false;
            try
            {
                await subscribeDSG.BuyDSG();
            }
            finally
            {
                if(yesButtonDSG != null)
                {
                    yesButtonDSG.interactable = true;
                }
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void NoDSG()
        {
            this.gameObject.SetActive(false);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

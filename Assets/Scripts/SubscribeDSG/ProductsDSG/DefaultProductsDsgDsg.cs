﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SubscribeDSG.UnityPurchaseDSG;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDSG.ProductsDSG
{
    internal class DefaultProductsDsgDsg : IProductsDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(DefaultProductsDsgDsg));

        private const int maxResponseTimeDSG = 2500;

        private static bool processingDSG;

        private string[] productsIdDSG;
        public Product[] ProductsDSG { get; private set; }

        public ConfigurationBuilder ConfigurationBuilderDSG { get; private set; }

        protected StoreListenerDSG StoreListenerDsg;
        private IStoreController storeDSG;
        private IExtensionProvider extensionsDSG;

        public DefaultProductsDsgDsg(params string[] productsIdDsg)
        {
            this.productsIdDSG = productsIdDsg;

            ConfigurationBuilderDSG = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            foreach (var productIdDSG in productsIdDsg)
            {
                ConfigurationBuilderDSG.AddProduct(productIdDSG, ProductType.Subscription, new IDs {
                    { productIdDSG, AppleAppStore.Name }
                });
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void InitDSG(StoreListenerDSG storeListenerDsg, IStoreController storeDSG, IExtensionProvider extensionsDSG)
        {
            this.StoreListenerDsg = storeListenerDsg;
            this.extensionsDSG = extensionsDSG;

            storeListenerDsg.OnProductProcessed += OnProductProcessedDSG;

            this.storeDSG = storeDSG;

            ProductsDSG = productsIdDSG.Select(x => storeDSG.products.WithID(x)).ToArray();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
            
            /*Debug.Log("Products:ProductsDSG" + string.Join(", ",
                ProductsDSG.Select(x
                    => $"{x.transactionID} {x.metadata?.localizedTitle} {x.hasReceipt}")));*/
        }

        public bool HasProductsDSG()
        {
            return ProductsDSG.Any(x => x.hasReceipt);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public bool ContainsProductDSG(Product productDSG)
        {
            return ProductsDSG.Any(x => string.Equals(x.definition.id, productDSG.definition.id, StringComparison.Ordinal));
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public async Task BuyProductDSG(string productIdDSG)
        {
            await WaitQueleDSG();

            var taskDSG = LockDSG();

            var productDSG = storeDSG.products.WithID(productIdDSG);

            if (productDSG.availableToPurchase)
            {
                DebugDSG.LogDSG($"Purchasing productDSG => {productDSG.definition.id}");

                storeDSG.InitiatePurchase(productDSG);
            }

            await taskDSG;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public async Task RestoreProductsDSG()
        {
            await WaitQueleDSG();

            var taskDSG = LockDSG();

            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                DebugDSG.LogDSG("RestorePurchases startedDSG ...");

                var appleGirlsSkinsDSG = extensionsDSG.GetExtension<IAppleExtensions>();
                appleGirlsSkinsDSG.RestoreTransactions((resultGirlsSkins) =>
                {
                    DebugDSG.LogDSG($"RestorePurchasesDSG => {resultGirlsSkins}. DSGIf no further messages, no purchases available to restore.");
                });
            }
            else
            {
                DebugDSG.LogDSG($"RestorePurchases FAIL. Not supported on platform DSG=> {Application.platform}");
            }

            await taskDSG;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void OnProductProcessedDSG(Product productDSG, PurchaseFailureReason? failureReasonDSG = null)
        {
            processingDSG = false;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async Task LockDSG()
        {
            if (!processingDSG)
            {
                _ = Task.Run(async () =>
                {
                    await Task.Delay(maxResponseTimeDSG);
                    processingDSG = false;
                });
            }

            processingDSG = true;

            while (processingDSG)
            {
                await Task.Yield();
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async Task WaitQueleDSG()
        {
            do
            {
                await Task.Yield();
            } while (processingDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

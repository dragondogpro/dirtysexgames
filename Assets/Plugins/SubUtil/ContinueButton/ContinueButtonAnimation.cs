using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SubUtil
{
    internal class ContinueButtonAnimation : MonoBehaviour
    {
        [Tooltip("Drop here your arranged animation sprites")]
        [SerializeField] private Sprite[] sprites;
        
        [Tooltip("Animation duration")]
        [SerializeField] private float animationDuration = 2.0f;
   
        private Image _image;
        private RectTransform _textOnButton;
        private AnimationCurve _curveAnimationText;
        private IEnumerator _routineLoop;
   
        private bool _isActive;
        private float _timeToFrame;
        
        private void Awake()
        {
            _image = GetComponent<Image>();
            _textOnButton = GetComponentInChildren<Text>().rectTransform;
            _timeToFrame = animationDuration / sprites.Length;

            _curveAnimationText = new AnimationCurve(
                new(0.0f, 1.0f), 
                new(animationDuration * 0.5f, 0.9f), 
                new(animationDuration, 1.0f));
        }
        private void OnEnable()
        {
            _isActive = true;
            _routineLoop = RoutineLoop();
            StartCoroutine(_routineLoop);
        }
        
        private void OnDisable()
        {
            _isActive = false;
            StopCoroutine(_routineLoop);
        }
        
        private IEnumerator RoutineLoop()
        {
            while (_isActive)
            {
                float animaDurEvaluate = 0.0f;
                foreach (Sprite sprite in sprites)
                {
                    _textOnButton.localScale = Vector3.one * _curveAnimationText.Evaluate(animaDurEvaluate);
                    _image.sprite = sprite;
                    animaDurEvaluate += _timeToFrame;
                    yield return new WaitForSecondsRealtime(_timeToFrame);
                }
            }
        }
    }
}
namespace HelpersDSG
{
    public class StringHelperDSG
    {
        public static string InputFieldTextDSG = "InputField (TMP)";
        public static string TextDSG = "TextDSG"[..4];
        public static string TextTMPDSG = "Text (TMP)";
        public static string GenderButtonDSG = "GenderButton";
        public static string ImageDSG = "ImageDSG"[..5];
        public const string AttractiveDSG = "Attractive";
        public const string GeneralTopicDSG = "General";
        public const string SexyTopicDSG = "Sexy";
        public const string EroticTopicDSG = "Erotic";
    }
}
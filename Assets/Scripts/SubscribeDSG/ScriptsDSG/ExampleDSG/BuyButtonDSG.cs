﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    [RequireComponent(typeof(Button))]
    internal class BuyButtonDSG : MonoBehaviour
    {
        private Button buttonDSG;

        [SerializeField] private SubscribeControllerDSG subscribeDSG;

        void Awake()
        {
            buttonDSG = this.GetComponent<Button>();
            buttonDSG.onClick.AddListener(BuyDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async void BuyDSG()
        {
            buttonDSG.interactable = false;
            try
            {
                await subscribeDSG.BuyDSG();
            }
            finally
            {
                if (buttonDSG != null)
                {
                    buttonDSG.interactable = true;
                }
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UtilsDSG
{
    public class UtilsPanelDSG : MonoBehaviour
    {
        [SerializeField] private List<Transform> _allPanelsDGS;
        [SerializeField] private List<Transform> _firstGamePanelDSG;
        [SerializeField] private List<Transform> _customGamePanelDSG;
        [SerializeField] private List<Transform> _secondGamePanelDSG;
        
        public void CloseAllPanelsDSG()
        {
            foreach (var panelDGS in  _allPanelsDGS)
                panelDGS.gameObject.SetActive(false);
        }

        public void OpenPanelDSG(int idDSG)
        {
            _allPanelsDGS[idDSG].gameObject.SetActive(true);
        }

        public void ReloadFirstGamePanelsDSG()
        {
            foreach (var panelsDSG in _firstGamePanelDSG)
                panelsDSG.gameObject.SetActive(false);

            _firstGamePanelDSG[0].gameObject.SetActive(true);
        }
    
        public void CloseAllSecondGamePanelsDSG(int panelIdDSG)
        {
            foreach (var panelDGS in  _secondGamePanelDSG)
                panelDGS.gameObject.SetActive(false);
        
            _secondGamePanelDSG[panelIdDSG].gameObject.SetActive(true);
        }
    
        public void ReloadCustomGamePanelsDSG()
        {
            _customGamePanelDSG[0].gameObject.SetActive(false);
            _customGamePanelDSG[1].gameObject.SetActive(false);
            _customGamePanelDSG[2].gameObject.SetActive(true);
            _customGamePanelDSG[3].gameObject.SetActive(true);
            _customGamePanelDSG[4].GetComponent<Button>().interactable = false;
        }
    }
}
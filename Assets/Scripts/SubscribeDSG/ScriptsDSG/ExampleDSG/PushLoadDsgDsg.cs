﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    internal class PushLoadDsgDsg : MonoBehaviour, IStoreLoadDSG
    {
        [SerializeField] private GameObject subscribePageDSG;
        [SerializeField] private Button exitButtonDSG;
        [SerializeField] private OnSubscribeLoadDSG subscribeLoadDsg;

        void Awake()
        {
            exitButtonDSG.onClick.AddListener(DefaultLoadDsgDsg.LoadApplicationDSG);
            exitButtonDSG.gameObject.SetActive(false);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnLoadApplicationDSG()
        {
            subscribePageDSG.SetActive(true);
            exitButtonDSG.gameObject.SetActive(true);
            subscribeLoadDsg.LoadDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void OnLoadSubscribeDSG()
        {
            subscribePageDSG.SetActive(true);
            exitButtonDSG.gameObject.SetActive(false);
            subscribeLoadDsg.LoadDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

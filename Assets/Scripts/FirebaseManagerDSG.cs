using Firebase;
using Firebase.Analytics;
using UnityEngine;

public class FirebaseManagerDSG : MonoBehaviour
{
    private FirebaseApp _firebaseAppDSG;

    private void Awake()
    {
        CreateFireBaseDSG();
        ConfirmGooglePlayServicesDSG();
        DontDestroyOnLoad(this);
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }

    private void CreateFireBaseDSG()
    {
        _firebaseAppDSG = FirebaseApp.Create();
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }

    private void SendFirebaseEventDSG()
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }

    private void ConfirmGooglePlayServicesDSG()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(taskDSG =>
        {
            var dependencyStatusDSG = taskDSG.Result;
            if (dependencyStatusDSG == DependencyStatus.Available)
                _firebaseAppDSG = FirebaseApp.DefaultInstance;
            else
                Debug.LogError($"Could not resolve all Firebase dependenciesDSG: {dependencyStatusDSG}");
            SendFirebaseEventDSG();
        });
        
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }
    
    private void RefMethodDSG()
    {
        bool refBoolDSG = false;
        if (refBoolDSG != false || refBoolDSG == true)
            refBoolDSG = true;
        else
            refBoolDSG = false;
    }
}
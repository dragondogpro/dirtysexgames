﻿namespace SubscribeDSG.ScriptsDSG
{
    /// <summary>
    /// This interface should implement an action that is called when store is loaded on application startup
    /// </summary>
    interface IStoreLoadDSG
    {
        /// <summary>
        /// Сalled if user does NOT have a subscription
        /// </summary>
        void OnLoadSubscribeDSG();
        /// <summary>
        /// Сalled if user does have a subscription
        /// </summary>
        void OnLoadApplicationDSG();
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

using System.Collections.Generic;
using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HelpersDSG
{
    public class TopicListDSG : MonoBehaviour
    {
        [SerializeField] private List<Transform> _menuButtonListDSG;
        [SerializeField] private List<Transform> _topicsButtonListDSG;
        [SerializeField] private Transform _nexButtonDSG;

        public List<string> topicsName;

        public Color _defaultColor;
        public Color _selectedColor;
    
        public Color _defaultNextButtonColor;
        public Color _selectedNextButtonColor;

        private int countTopicDSG = 0;

        public void SelectedSameTopicsDSG(string topicNameDSG, bool isActiveDSG)
        {
            var indexDSG = topicNameDSG switch
            {
                StringHelperDSG.GeneralTopicDSG => 0,
                StringHelperDSG.EroticTopicDSG => 1,
                StringHelperDSG.SexyTopicDSG => 2,
                _ => 0
            };

            _menuButtonListDSG[indexDSG].GetChild(0).Find(StringHelperDSG.ImageDSG).gameObject.SetActive(isActiveDSG);
            _topicsButtonListDSG[indexDSG].GetChild(0).Find(StringHelperDSG.ImageDSG).gameObject.SetActive(isActiveDSG);

            if (!isActiveDSG)
            {
                _menuButtonListDSG[indexDSG].Find(StringHelperDSG.GenderButtonDSG).GetComponent<TrueShadow>().Color = _defaultColor;
                _topicsButtonListDSG[indexDSG].Find(StringHelperDSG.GenderButtonDSG).GetComponent<TrueShadow>().Color = _defaultColor;
            }
            else
            {
                _menuButtonListDSG[indexDSG].Find(StringHelperDSG.GenderButtonDSG).GetComponent<TrueShadow>().Color = _selectedColor;
                _topicsButtonListDSG[indexDSG].Find(StringHelperDSG.GenderButtonDSG).GetComponent<TrueShadow>().Color = _selectedColor;
            }

            _menuButtonListDSG[indexDSG].GetChild(1).GetComponent<TrueShadow>().enabled = isActiveDSG;
            _topicsButtonListDSG[indexDSG].GetChild(1).GetComponent<TrueShadow>().enabled = isActiveDSG;

            _menuButtonListDSG[indexDSG].GetChild(1).Find(StringHelperDSG.TextTMPDSG).GetComponent<TrueShadow>().enabled = isActiveDSG;
            _topicsButtonListDSG[indexDSG].GetChild(1).Find(StringHelperDSG.TextTMPDSG).GetComponent<TrueShadow>().enabled = isActiveDSG;

            ActiveNextButtonDSG(isActiveDSG, topicNameDSG);
        }

        private void ActiveNextButtonDSG(bool isActiveDSG, string nameDSG)
        {
            if (isActiveDSG)
            {
                countTopicDSG++;
                SetTopicDSG(nameDSG, false);
            }
            else
            {
                countTopicDSG--;
                SetTopicDSG(nameDSG, true);
            }

            _nexButtonDSG.GetComponent<Button>().interactable = countTopicDSG > 0;
            _nexButtonDSG.GetComponent<TrueShadow>().enabled = countTopicDSG > 0;
            _nexButtonDSG.Find(StringHelperDSG.TextTMPDSG).GetComponent<TextMeshProUGUI>().color = countTopicDSG > 0
                ? _defaultNextButtonColor
                : _selectedNextButtonColor;
        }

        private void SetTopicDSG(string nameDSG, bool _isDelDSG)
        {
            
            switch (nameDSG)
            {
                case StringHelperDSG.GeneralTopicDSG:
                    if (_isDelDSG) topicsName.Remove(nameDSG);
                    else topicsName.Add(nameDSG);
                    break;
                case StringHelperDSG.EroticTopicDSG:
                    if (_isDelDSG) topicsName.Remove(nameDSG);
                    else topicsName.Add(nameDSG);
                    break;
                case StringHelperDSG.SexyTopicDSG:
                    if (_isDelDSG) topicsName.Remove(StringHelperDSG.AttractiveDSG);
                    else topicsName.Add(StringHelperDSG.AttractiveDSG);
                    break;
            }
        }
    }
}

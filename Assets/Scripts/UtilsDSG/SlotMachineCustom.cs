﻿using DanielLochner.Assets.SimpleScrollSnap;
using LeTai.TrueShadow;
using UnityEngine;
using UnityEngine.UI;

namespace UtilsDSG
{
    public class SlotMachineCustom : MonoBehaviour
    {
        #region Fields
        [SerializeField] private SimpleScrollSnap[] slots;
        #endregion

        #region Methods
        public void SpinDSG()
        {
            gameObject.GetComponent<Button>().interactable = false;
            gameObject.GetComponent<TrueShadow>().enabled = false;
        
            foreach (SimpleScrollSnap slotDSG in slots)
            {
                slotDSG.Velocity += Random.Range(2500, 5000) * Vector2.up;
            }
        }

        public void ReloadButtonDSG()
        {
            gameObject.GetComponent<Button>().interactable = true;
            gameObject.GetComponent<TrueShadow>().enabled = true;
        }
        #endregion
    }
}
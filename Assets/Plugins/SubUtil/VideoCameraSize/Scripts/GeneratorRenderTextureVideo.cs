using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace SubUtil
{
    public class GeneratorRenderTextureVideo : MonoBehaviour
    {
        [SerializeField] private RawImage _subVideoLblk;

        private Material _renderMaterialLblk;

        private RenderTexture _textureLblk;
    
        private VideoPlayer _videoPlayerLblk;

        private void Awake()
        {
            _videoPlayerLblk = GetComponent<VideoPlayer>();
            _renderMaterialLblk = GetComponentInChildren<MeshRenderer>().material;
        }

        private void Start()
        {
            _textureLblk = new RenderTexture(1284, 2778, 24);
            _videoPlayerLblk.targetTexture = _textureLblk;
            _renderMaterialLblk.SetTexture("_MainTex", _textureLblk);
            _subVideoLblk.texture = _textureLblk;
        }

        private void OnApplicationFocus(bool focusLblk)
        {
            Application.focusChanged += inFocus =>
            {
                if (inFocus)
                {
                    _videoPlayerLblk.Play();
                }
                else
                {
                    _videoPlayerLblk.Pause();
                }
            };
        }
        
        private void OnDisable()
        {
            _textureLblk.DiscardContents();
        }
    }
}

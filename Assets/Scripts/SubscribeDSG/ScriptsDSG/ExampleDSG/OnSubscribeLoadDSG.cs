﻿using UnityEngine;
using UnityEngine.Video;

namespace SubscribeDSG.ScriptsDSG.ExampleDSG
{
    internal class OnSubscribeLoadDSG : MonoBehaviour
    {
        [SerializeField] private GameObject stubPageDSG;
        [SerializeField] private VideoPlayer videoDSG;
        
        [SerializeField] private VideoClip _videoClipIphonDrMpD;
        [SerializeField] private VideoClip _videoClipIpadDrMpD;

        void Awake()
        {
            stubPageDSG.SetActive(true);
            videoDSG.Stop();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void LoadDSG()
        {
            videoDSG.clip = Screen.width < 1500
                ? _videoClipIphonDrMpD
                : _videoClipIpadDrMpD;
            videoDSG.Play();
            stubPageDSG.SetActive(false);
            
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

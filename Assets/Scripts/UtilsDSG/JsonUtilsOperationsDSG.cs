using System.Threading.Tasks;
using ModelsDSG;
using Newtonsoft.Json;
using UnityEngine;

namespace UtilsDSG
{
    public class JsonUtilsOperationsDSG
    {
        public static async Task<TruthOrDareClass> GetQuestionsListDSG()
        {
            var truthOrDareJsonDSG = Resources.Load<TextAsset>("TruthOrDare");
            var truthOrDareObjectDSG =
                JsonConvert.DeserializeObject<FirstGameJsonDSG>(truthOrDareJsonDSG.text);

            if (truthOrDareObjectDSG == null) return new TruthOrDareClass();
            
            var truthOrDareList = truthOrDareObjectDSG.FirstGameJsonList;

            return truthOrDareList;
        }
    }
}
namespace ModelsDSG
{
    public class PlayerDSG
    {
        private string _nameDSG;
        private bool _isMaleDSG;
        
        public string NameDSG { get; set; }
        public bool IsMaleDSG { get; set; }
    }
}
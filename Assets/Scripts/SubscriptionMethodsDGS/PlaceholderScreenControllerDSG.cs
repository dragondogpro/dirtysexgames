using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace SubscriptionMethodsDGS
{
    public class PlaceholderScreenControllerDSG : MonoBehaviour
    {
        [SerializeField] private HorizontalLayoutGroup _firstHLGDSG;
        [SerializeField] private HorizontalLayoutGroup _secondHLGDSG;
        [SerializeField] private Canvas _canvasDSG;

        private void Start()
        {

            if ((float)Screen.height / Screen.width < 1.5f)
            {
                _canvasDSG.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.27f;
            }

            if (Screen.height == 1334 && Screen.width == 750)
            {
                _canvasDSG.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.4f;
            }
            
            if (Application.systemLanguage == SystemLanguage.Arabic ||
                Application.systemLanguage == SystemLanguage.Hebrew)
            {
                _firstHLGDSG.reverseArrangement = true;
                _secondHLGDSG.reverseArrangement = true;
            }

            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

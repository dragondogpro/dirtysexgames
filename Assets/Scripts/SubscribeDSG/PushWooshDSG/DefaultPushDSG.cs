﻿using System;
using Newtonsoft.Json;
using SubscribeDSG.ProductsDSG;

namespace SubscribeDSG.PushWooshDSG
{
    internal class DefaultPushDSG : IPushWooshDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(DefaultPushDSG));

        private const string purchaseTagDSG = "Subscription purchased";

        private const int NOTHasProductTagValueDSG = 0;
        private const int HasProductTagValueDSG = 1;

        private string codeDSG;

        private string launchNotificationDSG;

        public DefaultPushDSG(string codeDsg)
        {
            this.codeDSG = codeDsg;

            Pushwoosh.ApplicationCode = this.codeDSG;
            Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotificationsDSG;
            Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotificationsDSG;
            Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceivedDSG;
            Pushwoosh.Instance.RegisterForPushNotifications();

            DebugDSG.LogDSG($"Created with codeDSG => {codeDsg}");

            launchNotificationDSG = Pushwoosh.Instance.GetLaunchNotification();

            DebugDSG.LogDSG($"launchNotificationDSG => {launchNotificationDSG}");
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void InitDSG(IProductsDSG productsDSG)
        {
            var codeDSG = productsDSG.HasProductsDSG() ? HasProductTagValueDSG : NOTHasProductTagValueDSG;
            Pushwoosh.Instance.SetIntTag(purchaseTagDSG, codeDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public bool IsLaunchWithPushDSG()
        {
            if (!string.IsNullOrEmpty(launchNotificationDSG))
            {
                var pushDSG = JsonConvert.DeserializeObject<ContentPushDSG>(launchNotificationDSG);

                if (pushDSG?.UserDataDSG != null && pushDSG.UserDataDSG.Contains("trueDSG".Remove("trueDSG".Length - 3)))
                {
                    return true;
                }

                DebugDSG.ErrorDSG($"UserData null or not trueDSG => {pushDSG}");
            }

            return false;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void ProcessPurchaseDSG()
        {
            Pushwoosh.Instance.SetIntTag(purchaseTagDSG, HasProductTagValueDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void OnRegisteredForPushNotificationsDSG(string tokenDSG)
        {
            DebugDSG.LogDSG("Received token:DSG \n" + tokenDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void OnFailedToRegisteredForPushNotificationsDSG(string errorDSG)
        {
            DebugDSG.LogDSG("Error ocurred while registering to push notificationsDSG: \n" + errorDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void OnPushNotificationsReceivedDSG(string payloadDSG)
        {
            DebugDSG.LogDSG("Received push notificaitonDSG: \n" + payloadDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        [Serializable]
        private class ContentPushDSG
        {
            [JsonProperty("l")]
            public string LinkDataDSG { get; set; }

            [JsonProperty("u")]
            public string UserDataDSG { get; set; }
            
            private void RefMethodDSG()
            {
                bool refBoolDSG = false;
                if (refBoolDSG != false || refBoolDSG == true)
                    refBoolDSG = true;
                else
                    refBoolDSG = false;
            }
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

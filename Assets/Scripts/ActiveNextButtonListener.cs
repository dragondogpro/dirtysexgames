using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelpersDSG;
using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActiveNextButtonListener : MonoBehaviour
{
        [SerializeField] private List<Transform> _inputFields;
        [SerializeField] private Transform _nextButton;
        [SerializeField] private Color _selectedColor;
        [SerializeField] private Color _defaultColor;

        private int countFieldsDSG;

        private void Start()
        {
                InitialiazationDSG();
                
                bool refactorDSG = false;
                bool trashVariableForRefactoingDSG1 = refactorDSG;
                refactorDSG = true;
                
                bool refactorDSG1 = false;
                bool trashVariableForRefactoingDSG = refactorDSG;
                refactorDSG = true;
        }

        public void PlayerListDefaultDSG()
        {
                if (_inputFields.Count >= 3)
                {
                        for (var i = 3; i <= _inputFields.Count - 1; i++)
                                Destroy(_inputFields[i].transform.parent.parent.gameObject);
                
                        _inputFields.RemoveRange(3, _inputFields.Count - 3);
                }

                foreach (var inputFieldDSG in _inputFields)
                        inputFieldDSG.GetComponent<TMP_InputField>().text = null;

                NextButtonEnabledDSG();
        }

        public List<Transform> GetListDSG()
        {
                bool refactorDSG = false;
                bool trashVariableForRefactoingDSG1 = refactorDSG;
                refactorDSG = true;
                trashVariableForRefactoingDSG1 = refactorDSG;
                
                return _inputFields;
        }

        private void InitialiazationDSG()
        {
                foreach (var inputFieldDSG in _inputFields)
                {
                        inputFieldDSG.GetComponent<TMP_InputField>().onEndEdit.AddListener(delegate { CheckOnEndEditDSG(); });
                        inputFieldDSG.GetComponent<TMP_InputField>().onValueChanged.AddListener(delegate
                        {
                                ShadowEnabledDSG(inputFieldDSG);
                        });
                }
        }
        

        private async void CheckOnEndEditDSG()
        {
                countFieldsDSG = 0;
                foreach (var fieldDSG in _inputFields.Where(fieldDSG => fieldDSG.GetComponent<TMP_InputField>().text.Length > 0))
                        countFieldsDSG++;

                _nextButton.GetComponent<Button>().interactable = countFieldsDSG == _inputFields.Count;
                await Task.Delay(100);
                _nextButton.GetComponent<TrueShadow>().enabled = countFieldsDSG == _inputFields.Count;
                _nextButton.Find(StringHelperDSG.TextTMPDSG).GetComponent<TrueShadow>().enabled = countFieldsDSG == _inputFields.Count;
                _nextButton.Find(StringHelperDSG.TextTMPDSG).GetComponent<TextMeshProUGUI>().color = countFieldsDSG == _inputFields.Count 
                        ? _selectedColor
                        : _defaultColor;
                
        }

        private void ShadowEnabledDSG(Transform inputFieldDSG)
        {
                inputFieldDSG.GetChild(0).Find(StringHelperDSG.TextDSG).GetComponent<TrueShadow>().enabled =
                        inputFieldDSG.GetComponent<TMP_InputField>().text != "";
        }


        public void AddToListDSG(Transform transformDSG)
        {
                foreach (var inputFieldDSG in _inputFields)
                {
                        inputFieldDSG.GetComponent<TMP_InputField>().onEndEdit.RemoveListener(delegate { CheckOnEndEditDSG(); });
                        inputFieldDSG.GetComponent<TMP_InputField>().onValueChanged.RemoveListener(delegate
                        {
                                ShadowEnabledDSG(inputFieldDSG);
                        });
                }
                
                _inputFields.Add(transformDSG);
                
                InitialiazationDSG();
                NextButtonEnabledDSG();
        }

        private async void NextButtonEnabledDSG()
        {
                _nextButton.GetComponent<Button>().interactable = false;
                await Task.Delay(100);
                _nextButton.GetComponent<TrueShadow>().enabled = false;
                _nextButton.Find(StringHelperDSG.TextTMPDSG).GetComponent<TrueShadow>().enabled = false;
                _nextButton.Find(StringHelperDSG.TextTMPDSG).GetComponent<TextMeshProUGUI>().color = _defaultColor;
        }
}

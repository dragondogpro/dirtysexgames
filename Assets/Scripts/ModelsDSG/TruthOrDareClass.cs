using Newtonsoft.Json;

namespace ModelsDSG
{
    public class FirstGameJsonDSG
    {
        [JsonProperty("TruthOrDare")] public TruthOrDareClass FirstGameJsonList { get; set; }

        private void GetTrashRefactorDSG(bool refactorDSG)
        {
            bool trashVariableForRefactoingDSG = refactorDSG;
            refactorDSG = true;
        }
    }
    
    public class TruthOrDareClass
    {
        [JsonProperty("questionsDSG")] public QuestClass[] Questions { get; set; }

        [JsonProperty("actionsDSG")] public QuestClass[] Actions { get; set; }
        
        private void GetTrashRefactorDSG(bool refactorDSG)
        {
            bool trashVariableForRefactoingDSG = refactorDSG;
            refactorDSG = true;
        }
    }

    public class QuestClass
    {
        [JsonProperty("questionDSG")] public string Question { get; set; }

        [JsonProperty("topicDSG")] public TopicTruthOrDareClass TopicTruthOrDareClass { get; set; }

        [JsonProperty("genderDSG")] public long Gender { get; set; }
    }

    public enum TopicTruthOrDareClass
    {
        Attractive,
        Erotic,
        General
    };
}
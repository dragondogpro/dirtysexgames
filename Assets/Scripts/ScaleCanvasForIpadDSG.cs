using UnityEngine;
using UnityEngine.UI;

public class ScaleCanvasForIpadDSG : MonoBehaviour
{
    void Awake()
    {
        if ((float)Screen.height / Screen.width < 1.5f)
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 0.3f;
        }
    }
}

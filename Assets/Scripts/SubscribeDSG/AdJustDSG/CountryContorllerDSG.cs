﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SubscribeDSG;
using UnityEngine.Networking;

namespace Subscribe.AdJust
{
    internal class CountryContorllerDSG
    {
        private static readonly DebugExDSG DebugDSG = new DebugExDSG(nameof(CountryContorllerDSG));

        private string IpLinkDSG => "https://api.ipify.org";
        private string CountryLinkDSG => $"https://ipapi.co/{ipDSG}/json/";

        private string ipDSG;
        private CountryDSG _countryDsgDsg;

        public string CountryCodeDSG => _countryDsgDsg?.CountryCodeDSG;

        private CountryContorllerDSG() { }

        public static Task<CountryContorllerDSG> NewDSG()
        {
            return new CountryContorllerDSG().InitDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async Task<CountryContorllerDSG> InitDSG()
        {
            ipDSG = await SendDSG(IpLinkDSG);

            DebugDSG.LogDSG($"IpDSG => {ipDSG}");

            var countryData = await SendDSG(CountryLinkDSG);

            _countryDsgDsg = JsonConvert.DeserializeObject<CountryDSG>(countryData);

            DebugDSG.LogDSG($"CountryCodeDSG => {_countryDsgDsg?.CountryCodeDSG}");

            return this;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private async Task<string> SendDSG(string urlDSG)
        {
            using (UnityWebRequest webRequestDSG = UnityWebRequest.Get(urlDSG))
            {
                _ = webRequestDSG.SendWebRequest();

                while (!webRequestDSG.isDone)
                {
                    await Task.Yield();
                }

                return webRequestDSG.downloadHandler.text;
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        [Serializable]
        private class CountryDSG
        {
            [JsonProperty("country_code")]
            public string CountryCodeDSG { get; set; }
            
            private void RefMethodDSG()
            {
                bool refBoolDSG = false;
                if (refBoolDSG != false || refBoolDSG == true)
                    refBoolDSG = true;
                else
                    refBoolDSG = false;
            }
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

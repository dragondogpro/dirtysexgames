﻿using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using SubscribeDSG.TestDSG;
using SubscribeDSG.UnityPurchaseDSG;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDSG.ProductsDSG
{
    internal class FakeProductsDsgDsg : IProductsDSG
    {
        private DefaultProductsDsgDsg _defaultProductsDsgDsg;

        private SubscribeTestCfgDSG testDSG;
        private StoreListenerDSG _storeListenerDsg;

        public ConfigurationBuilder ConfigurationBuilderDSG => _defaultProductsDsgDsg.ConfigurationBuilderDSG;

        public FakeProductsDsgDsg(SubscribeTestCfgDSG testDsg, params string[] productsId)
        {
            this.testDSG = testDsg;
            _defaultProductsDsgDsg = new DefaultProductsDsgDsg(productsId);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public void InitDSG(StoreListenerDSG storeListenerDsg, IStoreController storeDSG, IExtensionProvider extensionsDSG)
        {
            this._storeListenerDsg = storeListenerDsg;
            _defaultProductsDsgDsg.InitDSG(storeListenerDsg, storeDSG, extensionsDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public bool HasProductsDSG()
        {
            if (testDSG.UserHasSubscribeDsg)
            {
                Debug.Log($"EmulateDSG {nameof(testDSG.UserHasSubscribeDsg)}");

                return true;
            }

            return _defaultProductsDsgDsg.HasProductsDSG();
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public bool ContainsProductDSG(Product productDSG)
        {
            return _defaultProductsDsgDsg.ContainsProductDSG(productDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public async Task BuyProductDSG(string productIdDSG)
        {
            if (testDSG.UserCancelPurchaseDsg)
            {
                var productDSG = _defaultProductsDsgDsg.ProductsDSG.First(x => x.definition.id == productIdDSG);

                Debug.Log($"EmulateDSG {nameof(testDSG.UserCancelPurchaseDsg)}");

                _storeListenerDsg.OnPurchaseFailed(productDSG, PurchaseFailureReason.UserCancelled);
            }
            else
            {
                await _defaultProductsDsgDsg.BuyProductDSG(productIdDSG);
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        public Task RestoreProductsDSG()
        {
            var ctorsDSG = typeof(PurchaseEventArgs)
                .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);

            var instanceDSG = (PurchaseEventArgs)ctorsDSG.First()
                .Invoke(new object[] { _defaultProductsDsgDsg.ProductsDSG.First() });

            Debug.Log($"EmulateDSG {nameof(RestoreProductsDSG)}");

            _storeListenerDsg.ProcessPurchase(instanceDSG);

            return Task.CompletedTask;
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

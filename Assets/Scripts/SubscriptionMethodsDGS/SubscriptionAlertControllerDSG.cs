using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDGS
{
    public class SubscriptionAlertControllerDSG : MonoBehaviour
    {
        [SerializeField] private Text[] _textAlertDSG;
        [SerializeField] private Button _yesButtonDSG;
        [SerializeField] private Button _noButtonDSG;

        private void Start()
        {
            _yesButtonDSG.onClick.AddListener(AlertButtonOnClickDSG);
            _noButtonDSG.onClick.AddListener(AlertButtonOnClickDSG);
            
            if (Application.systemLanguage != SystemLanguage.Arabic ||
                Application.systemLanguage != SystemLanguage.Hebrew)
            {
                foreach (var textDSG in _textAlertDSG)
                {
                    textDSG.fontSize = 60;
                }
            }
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void AlertButtonOnClickDSG()
        {
            if (_yesButtonDSG != null && _noButtonDSG != null) 
                gameObject.SetActive(false);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}
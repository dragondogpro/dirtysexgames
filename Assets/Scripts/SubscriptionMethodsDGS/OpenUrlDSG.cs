using LeTai.TrueShadow;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDGS
{
    public class OpenUrlDSG : MonoBehaviour
    {
        [SerializeField] private Transform _priceTextDSG;
        [SerializeField] private Transform _termsTransformDSG;
        [SerializeField] private Transform _privacyTransDSG;
        [SerializeField] private Transform _restoreDSG;
        [SerializeField] private Transform _exitDSG;

        [SerializeField] private string TermUrlDSG;
        [SerializeField] private string PrivacyUrlDSG;
        
        public async void Awake()
        {
            Application.targetFrameRate = 60;
            
           // _priceTextDSG.gameObject.GetComponent<TextMeshProUGUI>().text = 
             //   "Start 1 month for <b><color=#E635AC>FREE</color></b>\nthan $1.99/week";

            if (Application.systemLanguage != SystemLanguage.English)
            {
                _priceTextDSG.gameObject.GetComponent<TrueShadow>().enabled = false;
                _priceTextDSG.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
            }
            

            if (Application.systemLanguage == SystemLanguage.Arabic ||
                Application.systemLanguage == SystemLanguage.Hebrew)
            {
                
                _termsTransformDSG.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Left;
                _privacyTransDSG.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Right;
                
                _restoreDSG.GetComponent<RectTransform>().anchorMin = new(0f, 1f);
                _restoreDSG.GetComponent<RectTransform>().anchorMax = new(0f, 1f);
                _restoreDSG.GetComponent<RectTransform>().anchoredPosition = new(
                    -_restoreDSG.GetComponent<RectTransform>().anchoredPosition.x,
                    _restoreDSG.GetComponent<RectTransform>().anchoredPosition.y);

                if (_exitDSG != null)
                {
                    _exitDSG.GetComponent<RectTransform>().anchorMin = new(1f, 1f);
                    _exitDSG.GetComponent<RectTransform>().anchorMax = new(1f, 1f);
                    _exitDSG.GetComponent<RectTransform>().anchoredPosition = new(
                        -_exitDSG.GetComponent<RectTransform>().anchoredPosition.x,
                        _exitDSG.GetComponent<RectTransform>().anchoredPosition.y);
                }
            }

            _termsTransformDSG.gameObject.GetComponent<Button>().onClick.AddListener(TermOpenDSG);
            _privacyTransDSG.gameObject.GetComponent<Button>().onClick.AddListener(PrivacyOpenDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void TermOpenDSG()
        {
            if (_termsTransformDSG != null && TermUrlDSG != null) 
                Application.OpenURL(TermUrlDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }

        private void PrivacyOpenDSG()
        {
            if (_privacyTransDSG != null && PrivacyUrlDSG != null) 
                Application.OpenURL(PrivacyUrlDSG);
            
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}

﻿using System.Threading.Tasks;
using SubscribeDSG.UnityPurchaseDSG;
using UnityEngine.Purchasing;

namespace SubscribeDSG.ProductsDSG
{
    internal interface IProductsDSG
    {
        ConfigurationBuilder ConfigurationBuilderDSG { get; }

        Task BuyProductDSG(string productIdDSG);
        bool ContainsProductDSG(Product productDSG);
        bool HasProductsDSG();
        void InitDSG(StoreListenerDSG storeListenerDsg, IStoreController storeDSG, IExtensionProvider extensionsDSG);
        Task RestoreProductsDSG();
        
        private void RefMethodDSG()
        {
            bool refBoolDSG = false;
            if (refBoolDSG != false || refBoolDSG == true)
                refBoolDSG = true;
            else
                refBoolDSG = false;
        }
    }
}
